# fid_widgets

Main library of FID - Flutter Israel Developers team.
This library includes all of the other libraries that will be
developed by team members.
*For contributing, read bellow*

## Screenshots
One function call to google login-logout    
<img src="https://gitlab.com/flutter-dev-israel/fid-widgets/-/raw/master/assets/gifs/easy_google_login.gif" alt="One function call to google login-logout" width="250"/>

Save any module easly to firestore   
<img src="https://gitlab.com/flutter-dev-israel/fid-widgets/-/raw/master/assets/gifs/easy_user_profile.gif" alt="Save any module easly to firestore" width="250"/>

Create image from local|URL with shape   
<img src="https://gitlab.com/flutter-dev-israel/fid-widgets/-/raw/master/assets/gifs/shaped_image.gif" alt="Create image from local|URL with shape" width="250"/>

Add any widget inside boarder with title   
<img src="https://gitlab.com/flutter-dev-israel/fid-widgets/-/raw/master/assets/gifs/titiled_boarder.gif" alt="Add any widget inside boarder with title" width="250"/>

Run popups from any place in the app   
<img src="https://gitlab.com/flutter-dev-israel/fid-widgets/-/raw/master/assets/gifs/popup.gif" alt="Run popups from any place in the app" width="250"/>

Easy way to create ExpandableListView  
<img src="https://gitlab.com/flutter-dev-israel/fid-widgets/-/raw/master/assets/gifs/expandable_list_view.gif" alt="Easy way to create ExpandableListView" width="250"/>

Responsive widgets to the screen size   
<img src="https://gitlab.com/flutter-dev-israel/fid-widgets/-/raw/master/assets/gifs/responsive_widgets.gif" alt="Responsive widgets to the screen size" width="250"/>


## How to Contribute to Exists/New Library? 
1. Clone this project.
2. Checkout to new branch with naming convention of: `dev/<your-name>/fid-<library-name>`.
3. Develop your library under `lib/src/fid-<lib-name>/<lib-dart-file>.dart`.
    You can run the app, by adding using your library in the main function at: `example/lib/main.dart`.
4. Add it to `lib/fid_widgets.dart` as: `export 'src/fid-<lib-name>/<lib-dart-file>.dart';`
5. Fix the version number in `./pubspec.yaml` file.
6. Open `CHANGELOG`, and write your changes there.
7. Open terminal and run:
```
> flutter pub publish --dry-run
Package has 0 warnings.
```
8. If in 4 the output show 0 errors/warnings, run the following(and authenticated if needed):
```
> flutter packages pub publish
...
Successfully uploaded package.
```
9. See the changes on [pub.dev](https://pub.dev/packages/fid_widgets)




