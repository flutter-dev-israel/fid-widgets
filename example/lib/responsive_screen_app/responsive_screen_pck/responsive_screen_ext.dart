import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';

///
/// The library use example.
///
class ResponsiveScreenExt {
  static final double heightMax = 667;
  static final double widthMax = 375;

  static final ResponsiveScreenExt _singleton = ResponsiveScreenExt._internal();

  factory ResponsiveScreenExt() {
    return _singleton;
  }

  ResponsiveScreenExt._internal();

  double heightResponse(BuildContext context, double height) {
    return ResponsiveScreen().heightResponsive(context, height, heightMax);
  }

  double widthResponse(BuildContext context, double width) {
    return ResponsiveScreen().widthResponsive(context, width, widthMax);
  }
}
