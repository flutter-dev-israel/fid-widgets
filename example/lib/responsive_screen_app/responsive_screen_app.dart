import 'package:flutter/material.dart';
import 'responsive_screen_pck/responsive_screen_ext.dart';

class ResponsiveScreenAppMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'ResponsiveScreen Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: ResponsiveScreenApp());
  }
}

class ResponsiveScreenApp extends StatefulWidget {
  ResponsiveScreenApp({Key key}) : super(key: key);

  @override
  ResponsiveScreenAppState createState() => ResponsiveScreenAppState();
}

class ResponsiveScreenAppState extends State<ResponsiveScreenApp> {
  ResponsiveScreenExt responsiveScreenExt = new ResponsiveScreenExt();

  ///
  /// The library use example.
  ///
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      height: responsiveScreenExt.heightResponse(context, 30),
                      width: responsiveScreenExt.widthResponse(context, 60),
                      decoration: new BoxDecoration(color: Color(0xffa7bbba))),
                  SizedBox(
                    height: responsiveScreenExt.heightResponse(context, 50),
                  ),
                  Container(
                      height: responsiveScreenExt.heightResponse(context, 50),
                      width: responsiveScreenExt.widthResponse(context, 150),
                      decoration: new BoxDecoration(color: Color(0xff2cbbba))),
                  SizedBox(
                    height: responsiveScreenExt.heightResponse(context, 50),
                  ),
                  Container(
                      height: responsiveScreenExt.heightResponse(context, 70),
                      width: responsiveScreenExt.widthResponse(context, 280),
                      decoration: new BoxDecoration(color: Color(0xffd9bbba))),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
