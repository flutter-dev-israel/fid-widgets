import 'package:example/google_login_app/google_login_app.dart';
import 'package:example/shaped_image_app/shaped_image_app.dart';
import 'package:example/titled_boarder_app/titled_boarder_app.dart';
import 'package:example/enum_picker_app/enum_picker_app.dart';
import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';
import 'package:example/expandable_list_view_app/expandable_list_view_app.dart';
import 'package:example/popup_app/popup_app.dart';

import 'firestore_document_app/firestore_document_app.dart';
import 'responsive_screen_app/responsive_screen_app.dart';

// void main() => runApp(PopupApp());

// void main() => runApp(ShapedImageApp());

// void main() => runApp(TitledBoarderApp());

// void main() => runApp(EnumPickerApp());

/* 
 * Brief:
 *  Easy way to manage login & logout of users via google and firebase.  
 * 
 * Example Requierments:
 * - set android variable 'minSdkVersion' to:  "minSdkVersion 21" 
 * - register your app to firebase(click on android icon on your online firebase project).
 * - enable google login on your online firebase project.
 */
// void main() => runApp(GoogleLoginApp());

/* 
 * Brief:
 *  Easy way to save user profile data(or other data) on firestore backend.
 * 
 * Example Requierments:
 * - Create database on firestore.
 * - On Firestore, enable the access to database via 'rules' tab: "allow read,...: if true"
 */
//void main() => runApp(FirestoreDocumentApp());

/* 
 * Brief:
 *  Easy way to create ExpandableListView.
 */
//void main() => runApp(ExpandableListViewApp());

/*
 * Brief:
 *  Responsive widgets to the screen size.
 */
void main() => runApp(ResponsiveScreenAppMain());
