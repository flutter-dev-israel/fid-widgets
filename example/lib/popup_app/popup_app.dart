import 'package:flutter/material.dart';
import 'package:fid_widgets/fid_widgets.dart';

class PopupApp extends StatefulWidget {
  @override
  _PopupAppAppState createState() => _PopupAppAppState();
}

class _PopupAppAppState extends State<PopupApp> {
  @override
  void dispose() {
    ///
    ///dont forget to dispose even if its last dispose of your app
    ///
    Popup.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      ///
      ///must add the builder function
      ///
      builder: Popup.builder,
      title: 'Popup Demo',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: PopupDemo(),
    );
  }
}

class PopupDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Popup Demo'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlatButton(
              child: Text("Test Popup"),
              color: Colors.lightBlueAccent,

              ///
              ///this is how you use it :)
              ///
              onPressed: () => Popup.popup(
                Container(
                  color: Colors.green,
                  padding: EdgeInsets.all(20),
                  child: Text("Popup by Gadi Perry"),
                ),
              ),
            ),
            FlatButton(
              child: Text("Navigate to another screen"),
              color: Colors.redAccent,

              ///
              ///use DismissPopupOnPop to close popup when backbutton is pressed otherwise navigator will pop but popup will remain visible
              ///optional property popAnyway (default:false)
              ///
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => DismissPopupOnPop(child: AnotherPage()),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AnotherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Popup Demo'),
        centerTitle: true,
      ),
      body: Center(
        child: FlatButton(
          child: Text("Test Popup"),
          color: Colors.lightGreenAccent,

          ///
          ///this is how you use it :)
          ///
          onPressed: () => Popup.popup(
            Container(
              color: Colors.blue,
              padding: EdgeInsets.all(20),
              child: Text("Popup by Gadi Perry"),
            ),
          ),
        ),
      ),
    );
  }
}
