import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';

class GoogleUserWidget extends StatelessWidget {
  final GoogleUserModule user;
  const GoogleUserWidget({Key key, @required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Google's Data:\n----------------"),
            Text("Email: ${user.email}"),
            Text("Profile:"),
            ShapedImage(
              imagePath: user.imageUrl,
              imageSource: ImageSourceType.Network,
              raduis: 50,
            ),
          ]),
    );
  }
}
