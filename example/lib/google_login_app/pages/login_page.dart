/*
sign in with google according to https://medium.com/flutter-community/flutter-implementing-google-sign-in-71888bca24ed
*/
import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';
import 'package:example/google_login_app/pages/home_page.dart';


class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            children: <Widget>[
               _signInButton(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _signInButton(BuildContext context) {
    return OutlineButton(
      onPressed: () {
        GoogleLoginAPI.signInWithGoogle().whenComplete(() {
          Navigator.of(context).pushReplacementNamed('/home');
        });
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ShapedImage(
            imagePath: 'assets/default_google_logo.png',
            imageSource: ImageSourceType.Local,
            raduis: 15,
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            'Sign in with Google',
            style: TextStyle(fontSize: 20, color: Colors.grey),
          )
        ],
      ),
    );
  }
}
