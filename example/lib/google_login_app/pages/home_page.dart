import 'package:example/google_login_app/widgets/google_user_widget.dart';
import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GoogleUserModule user = GoogleLoginAPI.getCurrentUser();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcom: ${user.name}"),
        actions: <Widget>[
          RaisedButton(
            onPressed: () {
              GoogleLoginAPI.signOutGoogle().whenComplete(
                (){
                  Navigator.of(context).pushReplacementNamed('/login'); 
                }
              );
            },
            child: Text("Logout"),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("User Logged In!"),
          GoogleUserWidget(user: user),
        ],
      ),
    );
  }
}
