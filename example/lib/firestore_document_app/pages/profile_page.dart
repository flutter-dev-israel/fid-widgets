/*
sign in with google according to https://medium.com/flutter-community/flutter-implementing-google-sign-in-71888bca24ed
*/
import 'dart:math';

import 'package:example/firestore_document_app/module/profile_user_module.dart';
import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';

const USER_ID = 'user-unique-id-just-for-example';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  /*
   * boolean indicator for loading user from firestore database 
   */
  bool isUserLoaded = false;

  /*
   * Controllers to get text from text input widgets
   */
  TextEditingController phoneController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  /*
   * - Use fid_widgets library for api to firestore for saving user profile. 
   * - Open collection for all users called "users" collection.
   */
  FirestoreDocumentAPI<ProfileUserModuleExample> firestoreApi =
      FirestoreDocumentAPI(collectionName: "users");

  /*
   * Initiate user module. It will be loaded from firestore.
   */
  ProfileUserModuleExample user = ProfileUserModuleExample(uid: USER_ID);

  @override
  Widget build(BuildContext context) {
    if (!isUserLoaded) {
      loadUser();
    }
    
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: !isUserLoaded
              ? Center(child: CircularProgressIndicator())
              : Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 10),
                    Text("Profile Data:"),
                    Text("Phone: ${user.phone}"),
                    Text("Description: ${user.description}"),
                    Text("user id: ${user.uid}"),
                    Text("\n----------------"),
                    SizedBox(height: 20),
                    Text("Edit Profile:"),
                    SizedBox(height: 20),
                    TitledBoarder(
                      title: Text("Phone"),
                      child: TextField(
                        controller: phoneController,
                      ),
                    ),
                    TitledBoarder(
                      title: Text("Description"),
                      child: TextField(
                        controller: descriptionController,
                      ),
                    ),
                    RaisedButton(
                      child: Text("Save"),
                      onPressed: () {
                        firestoreApi
                            .postModuleToFirestore(ProfileUserModuleExample(
                          uid: user.getID(),
                          phone: phoneController.text,
                          description: descriptionController.text,
                        ))
                            .then((updatedUser) {
                          setState(() {
                            user = updatedUser;
                          });
                        });
                      },
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  void loadUser() {
    firestoreApi.getModuleFromFirestore(user).then((userLoaded) {
      setState(() {
        if (user != null) {
          user = userLoaded;
        }
      });
    }).catchError((err) {
      print("user not exists");
    }).whenComplete(() {
      setState(() {
        isUserLoaded = true;
      });
    });
  }

  static String _generateUserID() {
    var random = Random.secure();
    return List<int>.generate(32, (i) => random.nextInt(256)).fold('',
        (current, newOne) {
      return current + newOne.toString();
    });
  }
}
