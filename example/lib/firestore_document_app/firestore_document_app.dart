import 'package:example/firestore_document_app/module/profile_user_module.dart';
import 'package:example/firestore_document_app/pages/profile_page.dart';
import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';

class FirestoreDocumentApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Shaped Image Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        routes: {
          '/profile': (context) => ProfilePage(),
        },
        /* 
         * Info: initial route is used instead of 'home' property 
         * Warning: When using initialRoute, don’t define a home property.
         */
        initialRoute: '/profile');
  }
}
