import 'package:flutter/material.dart';
import 'package:fid_widgets/fid_widgets.dart';

class DismissPopupOnPop extends StatelessWidget {
  final Widget child;
  final bool popAnyway;

  DismissPopupOnPop({@required this.child, this.popAnyway = false});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: child,
        onWillPop: () async {
          if (Popup.hasPopup) {
            Popup.dismiss();
            return popAnyway;
          }
          return true;
        });
  }
}
