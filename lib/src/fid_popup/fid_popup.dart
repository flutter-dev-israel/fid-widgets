import 'dart:async';
import 'package:flutter/material.dart';
export 'package:fid_widgets/src/fid_popup/fid_dismiss_popup_on_pop.dart';

class Popup extends StatelessWidget {
  final Widget child;

  Popup({@required this.child}) {
    if (_streamControler == null) {
      _streamControler = StreamController<Widget>.broadcast();
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          child,
          StreamBuilder<Widget>(
              stream: Popup._streamControler.stream,
              builder: (context, snapshot) {
                if (!snapshot.hasData) return SizedBox(height: 0);
                return GestureDetector(
                  onTap: Popup.dismiss,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.black.withOpacity(0.4),
                    child: GestureDetector(
                      onTap: () {},
                      child: Center(
                        child: snapshot.data,
                      ),
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }

  static Widget builder(BuildContext context, Widget child) =>
      Popup(child: child);

  static StreamController<Widget> _streamControler;

  static dispose() => _streamControler?.close();

  static popup(Widget child) {
    _streamControler.sink.add(child);
    hasPopup = true;
  }

  static dismiss() {
    popup(null);
    hasPopup = false;
  }

  static bool hasPopup = false;
}
