import 'package:cloud_firestore/cloud_firestore.dart';

abstract class DocumentModule {
  const DocumentModule();
  String getID();
  DocumentModule fromFirestore(DocumentSnapshot document);
  Map<String, String> toJson();
}
