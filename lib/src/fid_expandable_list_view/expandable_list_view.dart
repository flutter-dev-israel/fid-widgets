import 'package:flutter/material.dart';

// item_count - the count of the ExpandableListView
// index_top - the index of items of the ExpandableListView
// index_delimiter - the index of delimiter of the ExpandableListView
// title - the widget of the ExpandableListView
// child - the widget of the child of the ExpandableListView
// item_delimiter - the widget of the delimiter of the ExpandableListView

class ExpandableListView extends StatefulWidget {
  final int item_count;
  final int index_top;
  final Widget title;
  final Widget child;
  final int index_delimiter;
  final Widget item_delimiter;

  ExpandableListView(
      {Key key,
      @required this.item_count,
      this.index_top,
      @required this.title,
      @required this.child,
      this.index_delimiter,
      @required this.item_delimiter})
      : assert(index_top > 0),
        assert(index_delimiter > 0),
        super(key: key);

  @override
  State<StatefulWidget> createState() => ExpandableListViewState(
      index_top: index_top, index_delimiter: index_delimiter);
}

class ExpandableListViewState extends State<ExpandableListView> {
  int index_top;
  int index_delimiter;

  ExpandableListViewState({
    this.index_top,
    this.index_delimiter,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView.separated(
                    itemCount: widget.item_count,
                    itemBuilder: (BuildContext context, int index) {
                      index_top = index;
                      return ExpansionTile(
                        title: widget.title,
                        children: <Widget>[
                          widget.child,
                        ],
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      index_delimiter = index;
                      return widget.item_delimiter;
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
