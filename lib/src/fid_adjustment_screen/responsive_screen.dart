import 'package:flutter/material.dart';

// heightMediaQuery - adjust height to the screen size
// widthMediaQuery - adjust width to the screen size
class ResponsiveScreen {
  static final ResponsiveScreen _singleton = ResponsiveScreen._internal();

  factory ResponsiveScreen() {
    return _singleton;
  }

  ResponsiveScreen._internal();

  double heightResponsive(
      BuildContext context, double height, double heightMax) {
    return MediaQuery.of(context).size.height * height / heightMax;
  }

  double widthResponsive(BuildContext context, double width, double widthMax) {
    return MediaQuery.of(context).size.width * width / widthMax;
  }
}
