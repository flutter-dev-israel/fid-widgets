import 'package:flutter/material.dart';

typedef Render<ENUM> = Widget Function(ENUM);
typedef ChangeEvent<ENUM> = Function(ENUM);

class EnumPicker<ENUM> extends StatefulWidget {
  EnumPicker(List<ENUM> values, {Key key, Render<ENUM> render, ENUM value, ChangeEvent<ENUM> onChange }) :
        values = values,
        render = render,
        value = value,
        onChange = onChange,
        super(key: key) {
  }
  final List<ENUM> values;
  final Render<ENUM> render;
  final ChangeEvent<ENUM> onChange ;
  final ENUM value;

  Widget renderItem(ENUM value) {
    if ( render != null ) {
      return render(value);
    }
    return Text(value.toString());
  }

  @override
  EnumPickerState createState() {
    return EnumPickerState<ENUM>(
      values,
      value,
      (ENUM val) {
        return this.renderItem(val);
      },
      onChange
    );
  }
}

class EnumPickerState<ENUM> extends State< EnumPicker<ENUM> > {
  EnumPickerState(List<ENUM> values, ENUM value, Render<ENUM> render, ChangeEvent<ENUM> onChange ) {
    this.values = values;
    this.value = value;
    this.render = render;
    this.onChange = onChange;
  }

  Render<ENUM> render;
  ChangeEvent<ENUM> onChange;
  List<ENUM>   values;
  ENUM         value;

  update(ENUM newValue) {
    if ( this.value == newValue ) {
      return;
    }
    this.value = newValue;
    if ( onChange != null ) {
      onChange(this.value);
    }
    setState(() {
      this.value = value;
    });
  }

  @override
  void initState() {
    super.initState();
    if ( value == null ) {
      value = values[0];
    }
  }

  @override
  Widget build(BuildContext context) {
    DropdownButton ctl = DropdownButton<ENUM>(
      value: value,
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(
          color: Colors.black
      ),
      underline: Container(
        height: 2,
        color: Colors.black,
      ),
      onChanged: (ENUM newValue) {
        update(newValue);
      },
      items: values.map( (ENUM val) {
        return DropdownMenuItem<ENUM>(
          value: val,
          child: this.render(val),
        );
      }).toList()
    );
    return ctl;
  }
}
