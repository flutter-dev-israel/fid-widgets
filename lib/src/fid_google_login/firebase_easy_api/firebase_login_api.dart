import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fid_widgets/src/fid_google_login/modules/google_user_module.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:google_sign_in/google_sign_in.dart';

/*
sign in with google according to https://medium.com/flutter-community/flutter-implementing-google-sign-in-71888bca24ed
*/

// Add these three variables to store the info
// retrieved from the FirebaseUser
final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();
GoogleUserModule currentUserModule = GoogleUserModule();

class GoogleLoginAPI {
  final databaseReference = Firestore.instance;

  GoogleLoginAPI(
      {@required String userCollectionName}) {
    currentUserModule = GoogleUserModule();
  }

  static GoogleUserModule getCurrentUser() {
    return currentUserModule;
  }

  static Future<FirebaseUser> getCurrentUserIfExists() async {
    FirebaseUser _user = await FirebaseAuth.instance.currentUser();

    if (_user != null && !_user.isAnonymous) {
      print("User: ${_user.displayName ?? "None"}");
      return _user;
    }

    return Future.error("No User Found!");
  }

  static Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    //validate user values
    assert(currentUserModule.validateFirebaseUser(user));

    //load user from firebase
    currentUserModule = GoogleUserModule.fromFirebase(user);
    return 'signInWithGoogle succeeded: $user';
  }

  static Future signOutGoogle() async {
    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
    print("User Sign Out");
  }
  
}
